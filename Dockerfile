FROM node:8.12
RUN mkdir /oauth2
COPY ./oauth2-server-express /oauth2/oauth2-server-express
RUN (cd /oauth2/oauth2-server-express && npm install)
WORKDIR /oauth2/oauth2-server-express
CMD node ./bin/www >> /docker_logs/node.log 2>&1
