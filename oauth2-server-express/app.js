var createError = require("http-errors");
var cors = require("cors");
var express = require("express");
var session = require("express-session");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var sassMiddleware = require("node-sass-middleware");
var mongoose = require("mongoose");
var flash = require("connect-flash");
var envmap = require("./env");
var bodyParser = require("body-parser");

const current_env = envmap[process.env.BRANCH];
const axios = require("axios").default;

var app = express();

const cron = require("node-cron");

// NAGIOS CHECK HEARTBEAT
cron.schedule("0 */2 * * *", function () {
    if (current_env.isProduction) {
        axios
            .get(current_env.nagios_url, {
                auth: {
                    username: "agpassive",
                    password: "3nv3v3",
                },
                params: {
                    host: current_env.nagios_host,
                    service: `HeartBeat oauth2-server-${process.env.BRANCH}`,
                    status: "OK",
                    msg: "Heartbeat",
                },
            })
            .then((res) => {
                console.log(`${new Date()} - Heartbeat sent!`);
            })
            .catch((err) => {
                console.error(`${new Date()} - Heartbeat error!`);
            });
    }
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use(cors());
app.use(logger(`[:date[clf]] :method :url :status Content-Length: :res[content-length]`));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
    session({
        secret: current_env.salt,
        cookie: { maxAge: 60000 },
    })
);
app.use(cookieParser());
app.use(
    sassMiddleware({
        src: path.join(__dirname, "public"),
        dest: path.join(__dirname, "public"),
        indentedSyntax: false, // true = .sass and false = .scss
        sourceMap: true,
    })
);
app.use(flash());
app.use(express.static(path.join(__dirname, "public")));

// Database
if (current_env.isProduction) {
    mongoose.connect(current_env.mongoDbUrl, { useNewUrlParser: true });
} else {
    mongoose.connect(current_env.mongoDbUrl, { useNewUrlParser: true });
    mongoose.set("debug", true);
}
mongoose.set("useCreateIndex", true);
mongoose.set("useFindAndModify", false);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB error: "));
db.once("open", console.log.bind(console, "MongoDB connection successful"));

require("./models/user");
require("./models/oauth");

// Routes
app.use(require("./routes"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = !current_env.isProduction ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

// body parser
app.use(bodyParser.json({ limit: 10000000 }));

module.exports = app;
