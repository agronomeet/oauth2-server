const mongoose = require('mongoose');
const crypto = require('crypto');
const bcrypt = require('bcrypt');
const env = require('../env');
const ObjectID = mongoose.Schema.Types.ObjectId;
const _ = require('lodash');

let emailSchema = new mongoose.Schema(
	{
		address: { type: String, required: true, unique: true },
		verified: { type: Boolean },
	}
	// {
	// 	_id: false,
	// }
);

let UserSchema = new mongoose.Schema(
	{
		_id: { type: String },
		profile: {
			name: { type: String, default: null },
			role: { type: String, required: true },
			firstname: { type: String, default: null },
			lastname: { type: String, default: null },
			gender: { type: String, default: null },
			language: {
				_id: { type: String, default: 'en' },
				label: { type: String, default: 'English' },
				value: { type: String, default: 'en' },
			},
			image: { type: Object, default: null },
			groups: { type: [ObjectID], default: [] }, // usergroups the user is in
			crops: { type: [ObjectID], default: [] }, // crops created by user
			cropgroups: { type: [ObjectID], default: [] }, // cropgroups created by user
			mobile_number: { type: String, default: null },
			national_id: { type: String, default: null },
			adminArea: { type: String, default: null },
			agent: {
				farmerids: { type: [ObjectID], default: [] }, // if agent, Array of farmers user ids
				agentArea: { type: String, default: null },
				userGroupId: { type: String, default: null },
				cropGroupId: { type: String, default: null },
			},
			agentId: { type: String, default: null },
			superuser: { type: Boolean, default: null },
			certifier: { type: Boolean, default: null },
		},
		username: { type: String, unique: true, required: true },
		environments: { type: [String], default: [] },
		services: {
			password: {
				bcrypt: { type: String, required: true },
			},
		},
		emails: [emailSchema],

		// OAUTH FIELDS
		verificationCode: { type: String },
		verifiedAt: { type: Date },
	},
	{
		timestamps: true,
		// _id: false,
	}
);

UserSchema.methods.validatePassword = function (password) {
	// check password field
	password_match = false;
	if (this.password) {
		try {
			let _password = crypto
				.pbkdf2Sync(password, env.salt, 10000, 32, 'sha512')
				.toString('hex');
			password_match = this.password === _password;
		} catch (err) {
			return false;
		}
	}

	// check services.password field (meteor like)
	bcrypt_password_match = false;
	if (this.services && this.services.password && this.services.password.bcrypt) {
		try {
			hash = crypto.createHash('sha256').update(password).digest('hex');
			bcrypt_password_match = bcrypt.compareSync(hash, this.services.password.bcrypt);
		} catch (err) {
			return false;
		}
	}

	console.log(
		'UserSchema.methods.validatePassword',
		'password_match:',
		password_match,
		'bcrypt_password_match',
		bcrypt_password_match
	);
	return password_match || bcrypt_password_match;
};

UserSchema.methods.setPassword = function (password) {
	// set password field
	this.password = crypto
		.pbkdf2Sync(password, env[process.env.BRANCH].salt, 10000, 32, 'sha512')
		.toString('hex');

	// set services.password field (meteor like)
	hash = crypto.createHash('sha256').update(password).digest('hex');
	this.services.password.bcrypt = bcrypt.hashSync(hash, 10);
};

let UserModel = mongoose.model('User', UserSchema, 'users');

module.exports.addUser = async (userObject) => {
	return await UserModel.create(userObject);
};

module.exports.deleteUser = async (id) => {
	return await UserModel.findByIdAndDelete(id);
};

module.exports.deleteUsers = async (ids) => {
	return await UserModel.deleteMany({ _id: { $in: ids } });
};

module.exports.editUser = async (id, obj) => {
	return await UserModel.findByIdAndUpdate(id, obj, { new: true });
};

module.exports.getUserByID = async (id) => {
	return await UserModel.findById(id);
};

module.exports.getUser = async (query) => {
	return await UserModel.findOne(query);
};

module.exports.getUsers = async (query, offset, limit) => {
	return await UserModel.find(query)
		.collation({ locale: 'en', strength: 2 })
		.skip(offset ? parseInt(offset, 10) : 0)
		.limit(limit ? parseInt(limit, 10) : 1000)
		.sort({ 'profile.name': 1 });
};

module.exports.getUsersCount = async (query) => {
	return await UserModel.where(query).countDocuments();
};

module.exports.bulkUpsert = async (ops) => {
	try {
		ops.forEach(async (e) => {
			let res = await UserModel.updateOne(e.filter, { ...e.update }, { upsert: true });

			// we must update profile and profile.groups in two different queries because $set $setOnInsert $addToSet cannot modify the same object
			await UserModel.updateOne(e.filter, { ...e.after });
		});
		return 'ok';
	} catch (error) {
		console.log('ERROR', error);
		return error;
	}
};
