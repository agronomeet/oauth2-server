module.exports = {
    local: {
        isProduction: false,
        mongoDbUrl: "mongodb://172.31.0.17:27017/oauth",
        salt: "a5828e9d6052dc3b14a93e07a5932dd9" || process.env.salt,
        nagios_host: "agdev",
        nagios_url: "https://nagios.agronomeet.com/nagios/cgi-bin/ag-passive-check",
    },
    dev: {
        isProduction: process.env.NODE_ENV != "development",
        mongoDbUrl: process.env.MONGO_URL || "mongodb://172.31.0.17:27017/oauth",
        salt: "a5828e9d6052dc3b14a93e07a5932dd9" || process.env.salt,
        nagios_host: "agdev",
        nagios_url: "https://nagios.agronomeet.com/nagios/cgi-bin/ag-passive-check",
    },
    beta: {
        isProduction: process.env.NODE_ENV != "development",
        mongoDbUrl: process.env.MONGO_URL || "mongodb://172.30.0.17:27017/oauth",
        salt: "a5828e9d6052dc3b14a93e07a5932dd9" || process.env.salt,
        nagios_host: "agbeta",
        nagios_url: "https://nagios.agronomeet.com/nagios/cgi-bin/ag-passive-check",
    },
    master: {
        isProduction: process.env.NODE_ENV != "development",
        mongoDbUrl: process.env.MONGO_URL || "mongodb://172.18.0.17:27017/oauth",
        salt: "a5828e9d6052dc3b14a93e07a5932dd9" || process.env.salt,
        nagios_host: "Eve",
        nagios_url: "https://nagios.agronomeet.com/nagios/cgi-bin/ag-passive-check",
    },
};
