const router = require("express").Router();
const mongoose = require("mongoose");
const crypto = require("crypto");
const OAuthModel = require("../models/oauth");

router.get("/", (req, res, next) => {
    res.render("index", {
        title: "Node Express Example",
    });
});

// register OAuth Client
// router.get('/registerApiClient', (req, res, next) => {
// 	res.render('registerApiClient', { message: req.flash('message') });
// });

// router.post('/registerApiClient', async (req, res, next) => {
// 	let OAuthClientModel = mongoose.model('OAuthClient');

// 	// Create OAuth Client
// 	let _client = await OAuthModel.getClient(req.body.clientId, req.body.clientSecret);

// 	if (!_client) {
// 		_client = new OAuthClientModel({
// 			clientId: req.body.clientId,
// 			clientSecret: req.body.clientSecret,
// 			redirectUris: req.body.redirectUris.split(','),
// 			grants: ['authorization_code', 'client_credentials', 'refresh_token', 'password'],
// 		});
// 		_client.save();
// 	} else {
// 		return res.send('Client already existing!', 409);
// 	}

// 	req.flash('message', 'OAuth Client registration successful!');

// 	return res.redirect('/register');
// });

// register User
// router.get('/register', (req, res, next) => {
// 	res.render('register', { message: req.flash('message') });
// });

// router.post('/register', async (req, res, next) => {
// 	if (req.body.password !== req.body.confirmPassword) {
// 		return res.send('Passwords do not match!', 422);
// 	}

// 	let UserModel = mongoose.model('User');

// 	let user_name = {
// 		name: req.body.name || req.body.firstName + ' ' + req.body.lastName,
// 		...(req.body.firstName && { firstname: req.body.firstName }),
// 		...(req.body.lastName && { lastname: req.body.lastName }),
// 	};

// 	// Create User
// 	let _user = new UserModel({
// 		_id: new mongoose.Types.ObjectId().toHexString(),
// 		profile: {
// 			...user_name,
// 			role: req.body.role,
// 			language: req.body.language || { _id: 'en', label: 'English', value: 'en' },
// 		},
// 		username: req.body.email,
// 		environments: req.body.environments,
// 		emails: [
// 			{
// 				address: req.body.email,
// 				verified: true,
// 			},
// 		],
// 		verificationCode: crypto.randomBytes(16).toString('hex'),
// 	});

// 	let user = null;
// 	try {
// 		_user.setPassword(req.body.password);

// 		user = await _user.save();
// 	} catch (error) {
// 		return res.status(422).send('User validation failed.');
// 	}

// 	if (!user) {
// 		return res.status(422).send('Error creating user');
// 	}

// 	req.flash('message', 'User registration successful!');

// 	return res.send({ message: 'User registration successful!' });
// });

module.exports = router;
