const router = require("express").Router();
const OAuthServer = require("express-oauth-server");
const OAuthModel = require("../models/oauth");
const mongoose = require("mongoose");
var createError = require("http-errors");

let oauth = new OAuthServer({
    model: OAuthModel,
    debug: true,
});

let getAccessTokenLifetime = () => {
    switch (process.env.BRANCH) {
        case "local":
        case "dev":
            // 8 hours
            return 28800;
        case "master":
            // 4 hours
            return 14400;
        default:
            // default 60 minutes
            return 3600;
    }
};

router.post(
    "/oauth/access_token",
    oauth.token({
        requireClientAuthentication: {
            authorization_code: false,
            refresh_token: false,
        },
        accessTokenLifetime: getAccessTokenLifetime(),
    })
);

router.get("/oauth/authenticate", async (req, res, next) => {
    return res.render("authenticate");
});

router.post(
    "/oauth/authenticate",
    async (req, res, next) => {
        try {
            req.body.user = await OAuthModel.getUser(req.body.username, req.body.password);
            next();
        } catch (error) {
            next(error);
        }
    },
    oauth.authorize({
        authenticateHandler: {
            handle: (req) => {
                return req.body.user;
            },
        },
    })
);

module.exports = router;
