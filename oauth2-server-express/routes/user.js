const router = require('express').Router();
const OAuthServer = require('express-oauth-server');
const OAuthModel = require('../models/oauth');
const mongoose = require('mongoose');
const UserModel = require('../models/user');

let oauth = new OAuthServer({
	model: OAuthModel,
	useErrorHandler: true,
	debug: true,
});

// ADD
router.post('/user/add', oauth.authenticate(), async (req, res) => {
	try {
		let result = await UserModel.addUser(req.body);

		if (result) {
			res.send(result);
		} else {
			res.status(400).send({ message: 'Something occured. Please retry.' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

// ADD BULK
router.post('/user/add_bulk', oauth.authenticate(), async (req, res) => {
	try {
		let result = await UserModel.addUser(req.body);

		if (result) {
			res.send(result);
		} else {
			res.status(400).send({ message: 'Something occured. Please retry.' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

// EDIT
router.put('/user/edit/:id', oauth.authenticate(), async (req, res) => {
	if (!req.params.id) {
		res.status(400).send({ message: 'Please provide user ID.' });
	}

	try {
		let result = await UserModel.editUser(req.params.id, req.body);

		if (result) {
			return res.send(result);
		} else {
			return res.status(400).send({ message: 'User not found' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

// DELETE
router.delete('/user/delete/:id', oauth.authenticate(), async (req, res) => {
	if (!req.params.id || typeof req.params.id !== 'string') {
		res.status(400).send({ message: 'Please provide user ID.' });
	}

	try {
		let result = await UserModel.deleteUser(req.params.id);

		if (result) {
			return res.send(result);
		} else {
			return res.status(400).send({ message: 'User not found' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

// DELETE BULK
router.post('/user/delete_bulk', oauth.authenticate(), async (req, res) => {
	try {
		let result = await UserModel.deleteUsers(req.body);

		if (result) {
			res.send(req.body);
		} else {
			res.status(400).send({ message: 'Something occured. Please retry.' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

// GET ONE BY ID
router.get('/user/get/:id', oauth.authenticate(), async (req, res) => {
	if (!req.params.id) {
		res.status(400).send({ message: 'Please provide user ID.' });
	}

	try {
		let result = await UserModel.getUserByID(req.params.id);

		if (result) {
			return res.send(result);
		} else {
			return res.send({ message: 'User not found' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

// GET ONE BY QUERY
router.get('/user/get', oauth.authenticate(), async (req, res) => {
	if (!req.query.q) {
		res.status(400).send({ message: 'Please provide query.' });
	}

	try {
		let result = await UserModel.getUser(JSON.parse(req.query.q));

		if (result) {
			return res.send(result);
		} else {
			return res.send({ message: 'User not found' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

// GET MANY (BY QUERY)
router.get('/users/get', oauth.authenticate(), async (req, res) => {
	if (!req.query.q) {
		res.status(400).send({ message: 'Please provide query.' });
	}

	try {
		let result = await UserModel.getUsers(JSON.parse(req.query.q), req.query.o, req.query.l);

		if (result) {
			return res.send(result);
		} else {
			return res.send({ message: 'Users not found' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

// GET COUNT (BY QUERY)
router.get('/users/getCount', oauth.authenticate(), async (req, res) => {
	if (!req.query.q) {
		res.status(400).send({ message: 'Please provide query.' });
	}

	try {
		let result = await UserModel.getUsersCount(JSON.parse(req.query.q));

		if (result) {
			return res.send({ count: result });
		} else {
			return res.send({ message: 'Users not found' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

router.post('/users/upsert_bulk', oauth.authenticate(), async (req, res) => {
	try {
		let result = await UserModel.bulkUpsert(req.body);

		if (result) {
			res.send(result);
		} else {
			res.status(400).send({ message: 'Something occured. Please retry.' });
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;
