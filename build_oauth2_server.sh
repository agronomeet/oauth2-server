#!/bin/bash

# this script build and run docker for oauth2 server


curdir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P )

usage(){ echo "Usage: $0 [ master | beta | dev ]"; }

if [ $# -eq 0 ] ; then usage ; exit 1;  fi
branch="$1"
if [ "$branch" == "master" -o "$branch" == "beta" -o "$branch" == "dev" ] ; then echo "build for branch $branch" ; else usage ; exit 1; fi


docker_name=oauth2-server-${branch}
version=1
server=localhost


# mongo www:  172.18.0.17
# mongo beta: 172.30.0.17
# mongo dev:  172.31.0.17

server_oauth2_master=(    agwww    docker run -d --restart=always
    --network agnet_www --ip 172.18.0.5 --env PORT=3001
    --env MONGO_URL=mongodb://172.18.0.17:27017/oauth
    --env RABBIT_URL=amqp://172.18.0.30
    --env BRANCH=$branch
    -v /mnt/data/logs/$docker_name:/docker_logs
    --name=$docker_name localhost:5050/$docker_name:$version )

server_oauth2_beta=(    agbeta    docker run -d --restart=always
    --network agnet_beta --ip 172.30.0.5 --env PORT=3001
    --env MONGO_URL=mongodb://172.30.0.17:27017/oauth
    --env RABBIT_URL=amqp://172.30.0.30
    --env BRANCH=$branch
    -v /mnt/data/logs/$docker_name:/docker_logs
    --name=$docker_name localhost:5050/$docker_name:$version )

server_oauth2_dev=(    localhost    docker run -d --restart=always
    --network agnet_dev --ip 172.31.0.5 --env PORT=3001
    --env MONGO_URL=mongodb://172.31.0.17:27017/oauth
    --env RABBIT_URL=amqp://172.31.0.30
    --env BRANCH=$branch
    -v /home/enveve/logs/$docker_name:/docker_logs
    --name=$docker_name enveve/$docker_name:$version )



t() {
    echo "$@"
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "t: error with $1" >&2
        kill -s TERM $TOP_PID
        exit $status
    fi
    return $status
}
ts() {
    local RESULTS
    RESULTS=$("$@")
    local status=$?
    if [ $status -ne 0 ]; then
        echo "ts: error with $1" >&2
        kill -s TERM $TOP_PID
        exit $status
    fi
    return $status
}



docker_prepare(){
echo "----- docker prepare start --------------------------------"
echo "    - app: $app - branch: $branch - version: $version"
date "+%Y-%m-%d %H:%M:%S"

t cd ${curdir}
cat > Dockerfile << EOF
FROM node:8.12
RUN mkdir /oauth2
COPY ./oauth2-server-express /oauth2/oauth2-server-express
RUN (cd /oauth2/oauth2-server-express && npm install)
WORKDIR /oauth2/oauth2-server-express
CMD node ./bin/www >> /docker_logs/node.log 2>&1
EOF

date "+%Y-%m-%d %H:%M:%S"
echo "----- docker prepare end --------------------------------"
echo "" ; echo "" ; echo ""
}



docker_build(){
echo "----- docker build to local registry --------------------------------"
date "+%Y-%m-%d %H:%M:%S"
t cd ${curdir}
echo "----- docker build  ($(date +%H:%M:%S))";   t docker build -t enveve/$docker_name:$version .
echo "----- docker tag  ($(date +%H:%M:%S))";     t docker tag enveve/$docker_name:$version localhost:5050/$docker_name:$version
echo "----- docker push  ($(date +%H:%M:%S))";    t docker push localhost:5050/$docker_name:$version
echo "-----   clean up  ($(date +%H:%M:%S))";     docker rmi $(docker images -qa -f "dangling=true") > /dev/null 2>&1
echo "----- docker images  ($(date +%H:%M:%S))";  docker images
date "+%Y-%m-%d %H:%M:%S"
echo "----- docker build end --------------------------------"
echo "" ; echo "" ; echo ""
}



docker_deploy(){
local tmp=$1[@]
local arrArg=(${!tmp})
if [ ${#arrArg[*]} -ne 0 ]; then
    i=0
    server="${arrArg[$i]}";       i=$((i+1));
    docker_run="";
    for ((; i < ${#arrArg[*]}; i++)) {
        docker_run="$docker_run ${arrArg[$i]}"
    }
    echo "----- docker deploy $docker_name on $server --------------------------------"
    date "+%Y-%m-%d %H:%M:%S"
    if [ "${server}x" == "localhostx" ]; then
        echo "----- local deploy  -----"
        echo "----- docker stop  ($(date +%H:%M:%S))";    docker stop $docker_name
        echo "----- docker rm  ($(date +%H:%M:%S))";      docker rm $docker_name
        echo "-----   clean up  ($(date +%H:%M:%S))";     docker rmi $(docker images -qa -f "dangling=true") > /dev/null 2>&1
        echo "----- docker run  ($(date +%H:%M:%S))";     t $docker_run
    else
        echo "----- remote deploy, invoke pull via ssh tunnel  -----"
        echo "----- docker stop  ($(date +%H:%M:%S))";    ssh $server docker stop $docker_name
        echo "----- docker rm  ($(date +%H:%M:%S))";      ssh $server docker rm $docker_name
        echo "----- docker pull  ($(date +%H:%M:%S))";    ts ssh -g -R 5050:localhost:5050 $server docker pull localhost:5050/$docker_name:$version
        echo "-----   clean up  ($(date +%H:%M:%S))";     ssh $server docker rmi $(ssh $server docker images -qa -f "dangling=true") > /dev/null 2>&1
        echo "----- docker run  ($(date +%H:%M:%S))";     echo ssh $server $docker_run ; ts ssh $server $docker_run
    fi
    date "+%Y-%m-%d %H:%M:%S"
    echo "----- docker deploy end --------------------------------"
    echo ; echo ; echo
fi
}



docker_prepare
docker_build
docker_deploy server_oauth2_${branch}

