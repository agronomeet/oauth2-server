// External Dependancies
const boom = require('boom')

// Get Data Models
const Site = require('../models/Site')

// Get all sites
exports.getSites = async (req, reply) => {
    try {
        const sites = await Site.find()
        return sites
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Get single site by ID
exports.getSingleSite = async (req, reply) => {
    try {
        const id = req.params.id
        const site = await Site.findById(id)
        return site
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Add a new site
exports.addSite = async (req, reply) => {
    try {
        const site = new Site(req.body)
        return site.save()
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Update an existing site
exports.updateSite = async (req, reply) => {
    try {
        const id = req.params.id
        const site = req.body
        const { ...updateData } = site
        const update = await Site.findByIdAndUpdate(id, updateData, { new: true })
        return update
    } catch (err) {
        throw boom.boomify(err)
    }
}

// Delete a site
exports.deleteSite = async (req, reply) => {
    try {
        const id = req.params.id
        const site = await Site.findByIdAndRemove(id)
        return site
    } catch (err) {
        throw boom.boomify(err)
    }
}
