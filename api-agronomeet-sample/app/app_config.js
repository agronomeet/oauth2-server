// consider using .env

// default values
server_port = process.env.PORT || 5000;
mongo_url = process.env.MONGO_URL || "mongodb://172.31.0.17:27017/oauth";

exports.options = {
  server_port: server_port,

  mongo: {
    mongo_url: mongo_url,
  },

  log_level: "info",

  oauth2: {
    name: "enveveOAuth2",
    credentials: {
      client: {
        id: "api",
        secret: "secret",
      },
      auth: {
        authorizeHost: "http://localhost:3001",
        authorizePath: "/oauth/authenticate",
        tokenHost: "http://172.31.128.8:3001",
        tokenPath: "/oauth/access_token",
      },
    },
    // register a fastify url to start the redirect flow
    startRedirectPath: "/oauth/authenticate",
    // facebook redirect here after the user login
    callbackUri: "http://localhost:3002/oauth/callback",
    scope: "read",
  },

  swagger: {
    routePrefix: "/documentation",
    exposeRoute: true,
    swagger: {
      info: {
        title: "API Agronomeet",
        description: "REST API with Node.js, MongoDB, Fastify and Swagger",
        version: "1.0.0",
      },
      externalDocs: {
        url: "https://swagger.io",
        description: "Find more info here",
      },
      host: "localhost",
      schemes: ["http"],
      consumes: ["application/json"],
      produces: ["application/json"],
    },
  },
};
