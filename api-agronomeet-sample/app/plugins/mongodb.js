'use strict'

const fp = require('fastify-plugin')
const mongoose = require('mongoose')

module.exports = fp(async function (fastify, options) {
    mongoose.set('debug', true)
	// mongoose.set("debug", (collectionName, method, query, doc) => { console.log('${collectionName}.${method}', JSON.stringify(query), doc); });

	// Connect to DB
	mongoose.connect(options.mongo_url, { useNewUrlParser: true, useUnifiedTopology: true })
	    .then(() => console.log('MongoDB connected…'))
	    .catch(err => console.log(err))
})
