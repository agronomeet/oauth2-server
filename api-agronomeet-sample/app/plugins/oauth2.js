'use strict'

const fp = require('fastify-plugin')
const oauthPlugin = require('fastify-oauth2')

module.exports = fp(async function (fastify, options) {

	function verifyNumber (request, reply, done) {
	    request.log.info('fake verifyNumber')
	    return done()
	}

	function verifyOauth (request, reply, done) {
        request.log.info('verifyOauth')
        let bearerType = 'Bearer'
        const header = request.req.headers.authorization
        if (!header) {
            const noHeaderError = Error('missing authorization header')
            request.log.error('unauthorized: %s', noHeaderError.message)
            return done(noHeaderError)
        }

        const token = header.substring(bearerType.length).trim()
        request.log.info('verifyOauth oauth token', token)
        return done()
	}

	fastify.decorate('verifyNumber', verifyNumber)
	fastify.decorate('verifyOauth', verifyOauth)

	fastify.log.info('register oauth2 plugin with', options)
    fastify.register(oauthPlugin, options)

    fastify.get('/oauth/callback', async function (request, reply) {
        const token = await this.enveveOAuth2.getAccessTokenFromAuthorizationCodeFlow(request)
        fastify.log.info(token.access_token)

        // if later you need to refresh the token you can use
        // const newToken = await this.getNewAccessTokenUsingRefreshToken(token.refresh_token)

        reply.send({ access_token: token.access_token })
    })
})
