'use strict'

// Require config
const app_config = require('./app_config.js').options

// Require the framework and instantiate it
const fastify = require('fastify')({
    logger: {
        level: app_config.log_level
        // logfile : file: '/path/to/file' // will use pino.destination()
    }
})


// Plugins
fastify
    .register(require('./plugins/mongodb'), app_config.mongo)
    .register(require('./plugins/oauth2'), app_config.oauth2)
    .register(require('fastify-auth'))
    .after(routes)
    .register(require('fastify-swagger'), app_config.swagger)


// Routes
function routes () {
    const route_opts = require('./routes')
    // authenticated routes
    route_opts.authenticatedRoutes.forEach((route_opt, index) => {
        fastify.log.info('routes: auth route', route_opt)
        route_opt.preHandler = fastify.auth(
            [fastify.verifyNumber, fastify.verifyOdd, fastify.verifyOauth],
            { relation: 'and' }
        )
        fastify.route(route_opt)
    })
    // not authenticated routes
    route_opts.notAuthenticatedRoutes.forEach((route_opt, index) => {
        fastify.log.info('routes: not auth route', route_opt)
        fastify.route(route_opt)
    })
    return fastify
}


// Run the server!
const start = async () => {
    try {
        await fastify.listen(app_config.server_port, '0.0.0.0')
        fastify.swagger()
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}
start()
