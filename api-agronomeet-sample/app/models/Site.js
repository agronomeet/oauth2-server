// External Dependancies
const mongoose = require('mongoose')

const siteSchema = new mongoose.Schema({
    name: String,
    site_num: Number,
    latitude: String,
    longitude: String
    //rawserieids: [String],
    //rawserieids: { type: Array, of: String },
    //dataserieids: { type: Array, of: String }
})


module.exports = mongoose.model('Site', siteSchema, 'Site')
