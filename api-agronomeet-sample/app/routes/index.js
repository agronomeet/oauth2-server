
const indexRoutes = [
    {
        method: 'GET',
        url: '/',
        handler: function (request, reply) {
            reply.send({ hello: 'world' })
        }
    },
]

const loginRoutes = [
    {
        method: 'GET',
        url: '/login',
        handler: function (request, reply) {
            reply.send({ login: 'TODO' })
        }
    },
]

const siteRoutes = require('./siteRoutes')

module.exports = {
    authenticatedRoutes:[
        ...loginRoutes,
        ...siteRoutes
    ],

    notAuthenticatedRoutes:[
        ...indexRoutes
    ]
}
