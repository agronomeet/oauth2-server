const siteController = require('../controllers/siteController')

const routes = [
    {
        method: 'GET',
        url: '/api/sites',
        handler: siteController.getSites
    },
    {
        method: 'GET',
        url: '/api/sites/:id',
        handler: siteController.getSingleSite
    },
    {
        method: 'POST',
        url: '/api/sites',
        handler: siteController.addSite
        // TODO rivedi schema: documentation.addSiteSchema
    },
    {
        method: 'PUT',
        url: '/api/sites/:id',
        handler: siteController.updateSite
    },
    {
        method: 'DELETE',
        url: '/api/sites/:id',
        handler: siteController.deleteSite
    }
]

module.exports = routes
